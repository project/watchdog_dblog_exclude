<?php

function watchdog_dblog_exclude_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'watchdog') {
    // Remove the default dblog module hook so our function is fired instead.
    unset($implementations ['dblog']);
  }
}

/**
 * Implements hook_watchdog(),
 * hopefully overriding the one in the dblog module.
 */
function watchdog_dblog_exclude_watchdog(array $log_entry) {
  if(!watchdog_dblog_exclude_exclude($log_entry)) {
    Database::getConnection('default', 'default')->insert('watchdog')->fields(array(
    'uid' => $log_entry['uid'],
    'type' => substr($log_entry['type'], 0, 64),
    'message' => $log_entry['message'],
    'variables' => serialize($log_entry['variables']),
    'severity' => $log_entry['severity'],
    'link' => substr($log_entry['link'], 0, 255),
    'location' => $log_entry['request_uri'],
    'referer' => $log_entry['referer'],
    'hostname' => substr($log_entry['ip'], 0, 128),
    'timestamp' => $log_entry['timestamp'],
    ))->execute();
  }
}

/**
 * Function to check if log entry should be excluded.
 */
function watchdog_dblog_exclude_exclude($log_entry) {
  if(in_array($log_entry['type'], explode(',', variable_get('watchdog_dblog_exclude_types', '')))) {
    return TRUE;
  }
  $sevs = variable_get('watchdog_dblog_exclude_severity', '');
  if(isset($sevs[$log_entry['severity']])) {
    return TRUE;
  }
  if(in_array($log_entry['ip'], explode(',', variable_get('watchdog_dblog_exclude_ips', '')))) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter() for system_logging_settings().
 */
function watchdog_dblog_exclude_form_system_logging_settings_alter(&$form, $form_state) {
  $form['dblog_exclude_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Watchdog DBlog exclude settings'),
  );
  $form['dblog_exclude_settings']['watchdog_dblog_exclude_types'] = array(
    '#type' => 'textfield',
    '#title' => t('Types to exclude'),
    '#default_value' => variable_get('watchdog_dblog_exclude_types', ''),
    '#description' => t('Enter a comma separated list of types to exclude. (no spaces)'),
  );
  $form['dblog_exclude_settings']['watchdog_dblog_exclude_severity'] = array(
    '#type' => 'select',
    '#title' => t('Severity levels to exclude'),
    '#options' => array(
      WATCHDOG_EMERGENCY => t('Emergency'),
      WATCHDOG_ALERT => t('Alert'),
      WATCHDOG_CRITICAL => t('Critical'),
      WATCHDOG_ERROR => t('Error'),
      WATCHDOG_WARNING => t('Warning'),
      WATCHDOG_NOTICE => t('Notice'),
      WATCHDOG_INFO => t('Info'),
      WATCHDOG_DEBUG => t('Debug'),
    ),
    '#multiple' => TRUE,
    '#default_value' => variable_get('watchdog_dblog_exclude_severity', ''),
    '#desciption' => t('Severity levels to exclude.'),
  );
  $form['dblog_exclude_settings']['watchdog_dblog_exclude_ips'] = array(
    '#type' => 'textfield',
    '#title' => t('Ip addesses to exclude'),
    '#default_value' => variable_get('watchdog_dblog_exclude_ips', ''),
    '#description' => t('Enter a comma separated list of ips to exclude. (no spaces)'),
  );
}
